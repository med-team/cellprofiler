Source: cellprofiler
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <mathieu.malaterre@gmail.com>,
           Ivo Maintz <ivo@maintz.de>
Section: python
Priority: optional
Build-Depends: debhelper (>= 10),
               python-nose,
               cython,
               python-mysqldb,
               python-numpy,
               python-scipy,
               python-setuptools,
               python-matplotlib,
               python-wxgtk2.8,
               python-decorator,
               python-dev,
               default-jdk
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/med-team/cellprofiler
Vcs-Git: https://salsa.debian.org/med-team/cellprofiler.git
Homepage: http://www.cellprofiler.org

Package: cellprofiler
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python:Depends},
         cellprofiler-libs,
         default-jre | openjdk-6-jre | gcj-jre,
         jarwrapper | java-wrappers,
         python-decorator,
         python-h5py,
         python-libtiff,
         python-matplotlib,
         python-mysqldb,
         python-nose,
         python-numpy,
         python-scipy,
         python-wxgtk2.8,
         tifffile | python-tifffile
Recommends: tifffile
Provides: ${python:Provides}
Description: quantitatively measure phenotypes from images automatically
 CellProfiler is cell image analysis software designed to enable
 biologists without training in computer vision or programming to
 quantitatively measure phenotypes from thousands of images
 automatically.

Package: cellprofiler-libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python:Depends}
Description: libraries for cellprofiler
 CellProfiler is cell image analysis software designed to enable
 biologists without training in computer vision or programming to
 quantitatively measure phenotypes from thousands of images
 automatically.
 .
 This package ships the libraries for cellprofiler
